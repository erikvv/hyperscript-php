<?php

namespace Erikvv\HyperscriptTest;

use DOMDocument;
use Erikvv\Hyperscript\Hyperscript;
use PHPUnit\Framework\TestCase;
use Spatie\Regex\Regex;

class HyperscriptTest extends TestCase
{
    private static function stripWhitespaceBetweenElements(string $str)
    {
        $str = Regex::replace('/(\s+)</', '<', $str)->result();
        $str = Regex::replace('/>(\s+)/', '>', $str)->result();
        return $str;
    }

    private static function assertSameHtml(string $expected, string $actual): void
    {
        self::assertEquals(
            self::stripWhitespaceBetweenElements($expected),
            $actual
        );
    }

    public function testTwoChildren()
    {
        $h = new Hyperscript(new \DOMDocument());

        $element = $h('div',
            $h('div'),
            $h('div')
        );

        $result = $element->ownerDocument->saveHTML($element);

        self::assertSameHtml(
            '
                <div>
                    <div></div>
                    <div></div>
                </div>
            ',
            $result
        );
    }

    public function testFourSelectorsAndTwoChildren()
    {
        $h = new Hyperscript(new \DOMDocument());

        $element = $h('div#id1.class1.class2#id2',
            $h('div'),
            $h('div')
        );

        $result = $element->ownerDocument->saveHTML($element);

        self::assertSameHtml(
            '
                <div id="id1 id2" class="class1 class2">
                    <div></div>
                    <div></div>
                </div>
            ',
            $result
        );
    }

    public function testClassSelectorAndClassAttribute()
    {
        $h = new Hyperscript(new DOMDocument());

        $element = $h('div.class1',
              ['class' => 'class2'],
              $h('div')
        );

        $result = $element->ownerDocument->saveHTML($element);

        self::assertSameHtml(
            '
                <div class="class1 class2">
                    <div></div>
                </div>
            ',
            $result
        );
    }

    public function testUnsafe()
    {
        $h = new Hyperscript(new DOMDocument());

        $element = $h('div',
                      $h->unsafe('<script>alert(1)</script>')
        );

        $result = $element->ownerDocument->saveHTML($element);

        self::assertSameHtml(
            '
                <div>
                    <script>alert(1)</script>
                </div>
            ',
            $result
        );
    }

    public function testCallWithChild()
    {
        $h = new Hyperscript(new \DOMDocument());
        $element = $h->div($h->div());

        $result = $element->ownerDocument->saveHTML($element);

        self::assertSameHtml("
            <div>
                <div></div>
            </div>",
            $result
        );
    }

    public function testCallWithClassSelector()
    {
        $h = new Hyperscript(new \DOMDocument());
        $element = $h->div('.error');

        $result = $element->ownerDocument->saveHTML($element);

        self::assertSameHtml(
            '<div class="error"></div>',
            $result
        );
    }

    public function testFragment()
    {
        $h = new Hyperscript(new \DOMDocument());
        $fragment = $h->fragment(
            $h->p("1"),
            $h->p("2"),
        );

        $result = $fragment->ownerDocument->saveHTML($fragment);

        self::assertSameHtml(
            '<p>1</p><p>2</p>',
            $result
        );
    }
}
