<?php

// Helper to generate doc block for __call

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element
$tags = [
    'html',
    'base',
    'link',
    'meta',
    'style',
    'title',
    'body',

    // Content sectioning
    'address',
    'article',
    'aside',
    'footer',
    'header',
    'h1',
    'h2',
    'h3',
    'h4',
    'h5',
    'h6',
    'hgroup',
    'main',
    'nav',
    'section',

    // Text content
    'blockquote',
    'dd',
    'div',
    'dl',
    'dt',
    'figcaption',
    'figure',
    'hr',
    'li',
    'main',
    'ol',
    'p',
    'pre',
    'ul',

    // Inline text semantics
    'a',
    'abbr',
    'b',
    'bdi',
    'bdo',
    'br',
    'cite',
    'code',
    'data',
    'dfn',
    'em',
    'i',
    'kbd',
    'mark',
    'q',
    'rb',
    'rp',
    'rt',
    'rtc',
    'ruby',
    's',
    'samp',
    'small',
    'span',
    'strong',
    'sub',
    'sup',
    'time',
    'u',
    'var',
    'wbr',

    // Image and multimedia
    'area',
    'audio',
    'img',
    'map',
    'track',
    'video',

    // Embedded content
    'embed',
    'iframe',
    'object',
    'param',
    'picture',
    'source',

    // Scripting
    'canvas',
    'noscript',
    'script',

    // Demarcating edits
    'del',
    'ins',

    // Table content
    'caption',
    'col',
    'colgroup',
    'table',
    'tbody',
    'td',
    'tfoot',
    'th',
    'thead',
    'tr',

    // Forms
    'button',
    'datalist',
    'fieldset',
    'form',
    'input',
    'label',
    'legend',
    'meter',
    'optgroup',
    'option',
    'output',
    'progress',
    'select',
    'textarea',

    // Interactive elements
    'details',
    'dialog',
    'menu',
    'summary',

    // Web Components
    'slot',
    'template',
];

$template = <<<'TPL'
     * @method DOMElement %1$s(string $selector, string[] $attributes = null, string|DOMNode ...$children)
     * @method DOMElement %1$s(string $selector, string|DOMNode ...$children)
     * @method DOMElement %1$s(string|DOMNode ...$children)
     * @method DOMElement %1$s(string[] $attributes)
TPL;

foreach ($tags as $tag) {
    printf($template, $tag);
    echo "\n";
}
