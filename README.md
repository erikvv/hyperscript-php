Hyperscript for PHP
==============

HTML generation in pure PHP, replaces a template engine.

- [Advantages](#advantages)
- [Setup](#setup)
- [Background](#background)
- [Examples](#examples)
  - [Text fragment](#fragment)
  - [Form](#form)
  - [Escaping by default](#escaping-by-default)
  - [Disable escaping](#disable-escaping)
  - [Custom element](#custom-element)
  - [Custom component](#custom-component)

Advantages
--------

Advantages compared to a template engine:

- Easy to create components, using classes and functions
- No need to learn a DSL
- Safe from XSS
- IDE support

Setup
----

```
composer require erikvv/hyperscript
```

```php
use DOMDocument;
use Erikvv\Hyperscript\Hyperscript;

require './vendor/autoload.php';

$h = new Hyperscript(new DOMDocument());
```

Background
------

Hyperscript for PHP is built on top of the DOM extension. 

It is inspired by similar libraries such as [HyperScript for JS](https://github.com/hyperhype/hyperscript), [Hiccup for Clojure](https://github.com/weavejester/hiccup) and [Elm](https://package.elm-lang.org/packages/elm/html/latest)

Erikvv/hyperscript improves on [spatie/html-element](https://github.com/spatie/html-element) because that library is vulnerable to XSS.

Examples
------

<a name="fragment"></a>Text fragment:

```php
$fragment = $h->fragment(
    $h->h1('.welcome-header', 'Welcome'),
    $h->p('Write some hyperscript'),
);

echo $h->render($fragment);
```

```html
<h1 class="welcome-header">Welcome</h1>
<p>Write some Hyperscript</p>
```

<a name="form"></a>Form:

```php
$error = 'Cartoon birds not allowed';
$birdName = 'Donald Duck';

$element =
    $h->form(
        '#bird-form', [
            'method' => 'post',
            'action' => '/bird/submit',
        ],
        $error ? $h->div('.error', $error) : null,
        $h->div(
            $h->label(['for' => 'bird-name'], 'Name'),
            $h->input(
                '.bird-name', [
                    'name' => 'bird-name',
                    'value' => $birdName
                ]
            )
        ),
        $h->button('Submit')
    );

echo $h->render($element);
```

```html
<form id="bird-form" method="post" action="/bird/submit">
    <div class="error">Cartoon birds not allowed</div>
    <div>
        <label for="bird-name">Name</label>
        <input id="bird-name" name="bird-name" value="Donald Duck">
    </div>
    <button>Submit</button>
</form>
```

<a name="escaping-by-default"></a>Escaping by default:

```php
$element = $h->div('<script>alert(1)</script>');

echo $h->render($element);
```

```html
<div>&lt;script&gt;alert(1)&lt;/script&gt;</div>
```

<a name="disable-escaping"></a>Disable escaping:

```php
$element = $h->div(
    $h->unsafe('<script>alert(1)</script>')
);

echo $h->render($element);
```

```html
<div><script>alert(1)</script></div>
```

<a name="custom-element"></a>Custom element:

```php
$element = $h('my-custom-element', 'my-content')

echo $h->render($element);
```

```html
<my-custom-element>my-content</my-custom-element>
```

<a name="custom-component"></a>Custom component:

```php
/**
 * You can improve this class with object-oriented 
 * or functional techniques to fit your usecase.
 */
class RoundedMillimeterTable
{
    public static function row(Hyperscript $h, string $rowHeader, float ...$values): DOMElement 
    {
        $cells = [
            $h->td($rowHeader)
        ];

        foreach ($values as $value) {
            $cells[] = $h->td(number_format($value, 1) . ' mm');
        }

        return $h->tr(...$cells);
    }
}

$row = RoundedMillimeterTable::row($h, 'displacement', 1.8, 2.0);

echo $h->render($row);
```

```
<tr>
    <td>displacement</td>
    <td>1.8 mm</td>
    <td>2.0 mm</td>
</tr>
```
