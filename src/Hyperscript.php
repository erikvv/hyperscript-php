<?php

namespace Erikvv\Hyperscript;

use DOMDocument;
use DOMDocumentFragment;
use DOMElement;
use DOMNode;
use DOMText;
use InvalidArgumentException;
use Spatie\Regex\Regex;

/**
 * @method DOMElement html(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement html(string $selector, string|DOMElement ...$children)
 * @method DOMElement html(string|DOMElement ...$children)
 * @method DOMElement html(string[] $attributes)
 * @method DOMElement base(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement base(string $selector, string|DOMElement ...$children)
 * @method DOMElement base(string|DOMElement ...$children)
 * @method DOMElement base(string[] $attributes)
 * @method DOMElement link(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement link(string $selector, string|DOMElement ...$children)
 * @method DOMElement link(string|DOMElement ...$children)
 * @method DOMElement link(string[] $attributes)
 * @method DOMElement meta(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement meta(string $selector, string|DOMElement ...$children)
 * @method DOMElement meta(string|DOMElement ...$children)
 * @method DOMElement meta(string[] $attributes)
 * @method DOMElement style(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement style(string $selector, string|DOMElement ...$children)
 * @method DOMElement style(string|DOMElement ...$children)
 * @method DOMElement style(string[] $attributes)
 * @method DOMElement title(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement title(string $selector, string|DOMElement ...$children)
 * @method DOMElement title(string|DOMElement ...$children)
 * @method DOMElement title(string[] $attributes)
 * @method DOMElement body(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement body(string $selector, string|DOMElement ...$children)
 * @method DOMElement body(string|DOMElement ...$children)
 * @method DOMElement body(string[] $attributes)
 * @method DOMElement address(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement address(string $selector, string|DOMElement ...$children)
 * @method DOMElement address(string|DOMElement ...$children)
 * @method DOMElement address(string[] $attributes)
 * @method DOMElement article(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement article(string $selector, string|DOMElement ...$children)
 * @method DOMElement article(string|DOMElement ...$children)
 * @method DOMElement article(string[] $attributes)
 * @method DOMElement aside(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement aside(string $selector, string|DOMElement ...$children)
 * @method DOMElement aside(string|DOMElement ...$children)
 * @method DOMElement aside(string[] $attributes)
 * @method DOMElement footer(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement footer(string $selector, string|DOMElement ...$children)
 * @method DOMElement footer(string|DOMElement ...$children)
 * @method DOMElement footer(string[] $attributes)
 * @method DOMElement header(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement header(string $selector, string|DOMElement ...$children)
 * @method DOMElement header(string|DOMElement ...$children)
 * @method DOMElement header(string[] $attributes)
 * @method DOMElement h1(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement h1(string $selector, string|DOMElement ...$children)
 * @method DOMElement h1(string|DOMElement ...$children)
 * @method DOMElement h1(string[] $attributes)
 * @method DOMElement h2(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement h2(string $selector, string|DOMElement ...$children)
 * @method DOMElement h2(string|DOMElement ...$children)
 * @method DOMElement h2(string[] $attributes)
 * @method DOMElement h3(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement h3(string $selector, string|DOMElement ...$children)
 * @method DOMElement h3(string|DOMElement ...$children)
 * @method DOMElement h3(string[] $attributes)
 * @method DOMElement h4(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement h4(string $selector, string|DOMElement ...$children)
 * @method DOMElement h4(string|DOMElement ...$children)
 * @method DOMElement h4(string[] $attributes)
 * @method DOMElement h5(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement h5(string $selector, string|DOMElement ...$children)
 * @method DOMElement h5(string|DOMElement ...$children)
 * @method DOMElement h5(string[] $attributes)
 * @method DOMElement h6(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement h6(string $selector, string|DOMElement ...$children)
 * @method DOMElement h6(string|DOMElement ...$children)
 * @method DOMElement h6(string[] $attributes)
 * @method DOMElement hgroup(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement hgroup(string $selector, string|DOMElement ...$children)
 * @method DOMElement hgroup(string|DOMElement ...$children)
 * @method DOMElement hgroup(string[] $attributes)
 * @method DOMElement main(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement main(string $selector, string|DOMElement ...$children)
 * @method DOMElement main(string|DOMElement ...$children)
 * @method DOMElement main(string[] $attributes)
 * @method DOMElement nav(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement nav(string $selector, string|DOMElement ...$children)
 * @method DOMElement nav(string|DOMElement ...$children)
 * @method DOMElement nav(string[] $attributes)
 * @method DOMElement section(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement section(string $selector, string|DOMElement ...$children)
 * @method DOMElement section(string|DOMElement ...$children)
 * @method DOMElement section(string[] $attributes)
 * @method DOMElement blockquote(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement blockquote(string $selector, string|DOMElement ...$children)
 * @method DOMElement blockquote(string|DOMElement ...$children)
 * @method DOMElement blockquote(string[] $attributes)
 * @method DOMElement dd(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement dd(string $selector, string|DOMElement ...$children)
 * @method DOMElement dd(string|DOMElement ...$children)
 * @method DOMElement dd(string[] $attributes)
 * @method DOMElement div(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement div(string $selector, string|DOMElement ...$children)
 * @method DOMElement div(string|DOMElement ...$children)
 * @method DOMElement div(string[] $attributes)
 * @method DOMElement dl(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement dl(string $selector, string|DOMElement ...$children)
 * @method DOMElement dl(string|DOMElement ...$children)
 * @method DOMElement dl(string[] $attributes)
 * @method DOMElement dt(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement dt(string $selector, string|DOMElement ...$children)
 * @method DOMElement dt(string|DOMElement ...$children)
 * @method DOMElement dt(string[] $attributes)
 * @method DOMElement figcaption(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement figcaption(string $selector, string|DOMElement ...$children)
 * @method DOMElement figcaption(string|DOMElement ...$children)
 * @method DOMElement figcaption(string[] $attributes)
 * @method DOMElement figure(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement figure(string $selector, string|DOMElement ...$children)
 * @method DOMElement figure(string|DOMElement ...$children)
 * @method DOMElement figure(string[] $attributes)
 * @method DOMElement hr(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement hr(string $selector, string|DOMElement ...$children)
 * @method DOMElement hr(string|DOMElement ...$children)
 * @method DOMElement hr(string[] $attributes)
 * @method DOMElement li(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement li(string $selector, string|DOMElement ...$children)
 * @method DOMElement li(string|DOMElement ...$children)
 * @method DOMElement li(string[] $attributes)
 * @method DOMElement main(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement main(string $selector, string|DOMElement ...$children)
 * @method DOMElement main(string|DOMElement ...$children)
 * @method DOMElement main(string[] $attributes)
 * @method DOMElement ol(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement ol(string $selector, string|DOMElement ...$children)
 * @method DOMElement ol(string|DOMElement ...$children)
 * @method DOMElement ol(string[] $attributes)
 * @method DOMElement p(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement p(string $selector, string|DOMElement ...$children)
 * @method DOMElement p(string|DOMElement ...$children)
 * @method DOMElement p(string[] $attributes)
 * @method DOMElement pre(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement pre(string $selector, string|DOMElement ...$children)
 * @method DOMElement pre(string|DOMElement ...$children)
 * @method DOMElement pre(string[] $attributes)
 * @method DOMElement ul(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement ul(string $selector, string|DOMElement ...$children)
 * @method DOMElement ul(string|DOMElement ...$children)
 * @method DOMElement ul(string[] $attributes)
 * @method DOMElement a(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement a(string $selector, string|DOMElement ...$children)
 * @method DOMElement a(string|DOMElement ...$children)
 * @method DOMElement a(string[] $attributes)
 * @method DOMElement abbr(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement abbr(string $selector, string|DOMElement ...$children)
 * @method DOMElement abbr(string|DOMElement ...$children)
 * @method DOMElement abbr(string[] $attributes)
 * @method DOMElement b(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement b(string $selector, string|DOMElement ...$children)
 * @method DOMElement b(string|DOMElement ...$children)
 * @method DOMElement b(string[] $attributes)
 * @method DOMElement bdi(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement bdi(string $selector, string|DOMElement ...$children)
 * @method DOMElement bdi(string|DOMElement ...$children)
 * @method DOMElement bdi(string[] $attributes)
 * @method DOMElement bdo(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement bdo(string $selector, string|DOMElement ...$children)
 * @method DOMElement bdo(string|DOMElement ...$children)
 * @method DOMElement bdo(string[] $attributes)
 * @method DOMElement br(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement br(string $selector, string|DOMElement ...$children)
 * @method DOMElement br(string|DOMElement ...$children)
 * @method DOMElement br(string[] $attributes)
 * @method DOMElement cite(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement cite(string $selector, string|DOMElement ...$children)
 * @method DOMElement cite(string|DOMElement ...$children)
 * @method DOMElement cite(string[] $attributes)
 * @method DOMElement code(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement code(string $selector, string|DOMElement ...$children)
 * @method DOMElement code(string|DOMElement ...$children)
 * @method DOMElement code(string[] $attributes)
 * @method DOMElement data(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement data(string $selector, string|DOMElement ...$children)
 * @method DOMElement data(string|DOMElement ...$children)
 * @method DOMElement data(string[] $attributes)
 * @method DOMElement dfn(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement dfn(string $selector, string|DOMElement ...$children)
 * @method DOMElement dfn(string|DOMElement ...$children)
 * @method DOMElement dfn(string[] $attributes)
 * @method DOMElement em(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement em(string $selector, string|DOMElement ...$children)
 * @method DOMElement em(string|DOMElement ...$children)
 * @method DOMElement em(string[] $attributes)
 * @method DOMElement i(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement i(string $selector, string|DOMElement ...$children)
 * @method DOMElement i(string|DOMElement ...$children)
 * @method DOMElement i(string[] $attributes)
 * @method DOMElement kbd(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement kbd(string $selector, string|DOMElement ...$children)
 * @method DOMElement kbd(string|DOMElement ...$children)
 * @method DOMElement kbd(string[] $attributes)
 * @method DOMElement mark(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement mark(string $selector, string|DOMElement ...$children)
 * @method DOMElement mark(string|DOMElement ...$children)
 * @method DOMElement mark(string[] $attributes)
 * @method DOMElement q(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement q(string $selector, string|DOMElement ...$children)
 * @method DOMElement q(string|DOMElement ...$children)
 * @method DOMElement q(string[] $attributes)
 * @method DOMElement rb(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement rb(string $selector, string|DOMElement ...$children)
 * @method DOMElement rb(string|DOMElement ...$children)
 * @method DOMElement rb(string[] $attributes)
 * @method DOMElement rp(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement rp(string $selector, string|DOMElement ...$children)
 * @method DOMElement rp(string|DOMElement ...$children)
 * @method DOMElement rp(string[] $attributes)
 * @method DOMElement rt(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement rt(string $selector, string|DOMElement ...$children)
 * @method DOMElement rt(string|DOMElement ...$children)
 * @method DOMElement rt(string[] $attributes)
 * @method DOMElement rtc(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement rtc(string $selector, string|DOMElement ...$children)
 * @method DOMElement rtc(string|DOMElement ...$children)
 * @method DOMElement rtc(string[] $attributes)
 * @method DOMElement ruby(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement ruby(string $selector, string|DOMElement ...$children)
 * @method DOMElement ruby(string|DOMElement ...$children)
 * @method DOMElement ruby(string[] $attributes)
 * @method DOMElement s(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement s(string $selector, string|DOMElement ...$children)
 * @method DOMElement s(string|DOMElement ...$children)
 * @method DOMElement s(string[] $attributes)
 * @method DOMElement samp(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement samp(string $selector, string|DOMElement ...$children)
 * @method DOMElement samp(string|DOMElement ...$children)
 * @method DOMElement samp(string[] $attributes)
 * @method DOMElement small(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement small(string $selector, string|DOMElement ...$children)
 * @method DOMElement small(string|DOMElement ...$children)
 * @method DOMElement small(string[] $attributes)
 * @method DOMElement span(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement span(string $selector, string|DOMElement ...$children)
 * @method DOMElement span(string|DOMElement ...$children)
 * @method DOMElement span(string[] $attributes)
 * @method DOMElement strong(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement strong(string $selector, string|DOMElement ...$children)
 * @method DOMElement strong(string|DOMElement ...$children)
 * @method DOMElement strong(string[] $attributes)
 * @method DOMElement sub(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement sub(string $selector, string|DOMElement ...$children)
 * @method DOMElement sub(string|DOMElement ...$children)
 * @method DOMElement sub(string[] $attributes)
 * @method DOMElement sup(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement sup(string $selector, string|DOMElement ...$children)
 * @method DOMElement sup(string|DOMElement ...$children)
 * @method DOMElement sup(string[] $attributes)
 * @method DOMElement time(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement time(string $selector, string|DOMElement ...$children)
 * @method DOMElement time(string|DOMElement ...$children)
 * @method DOMElement time(string[] $attributes)
 * @method DOMElement u(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement u(string $selector, string|DOMElement ...$children)
 * @method DOMElement u(string|DOMElement ...$children)
 * @method DOMElement u(string[] $attributes)
 * @method DOMElement var(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement var(string $selector, string|DOMElement ...$children)
 * @method DOMElement var(string|DOMElement ...$children)
 * @method DOMElement var(string[] $attributes)
 * @method DOMElement wbr(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement wbr(string $selector, string|DOMElement ...$children)
 * @method DOMElement wbr(string|DOMElement ...$children)
 * @method DOMElement wbr(string[] $attributes)
 * @method DOMElement area(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement area(string $selector, string|DOMElement ...$children)
 * @method DOMElement area(string|DOMElement ...$children)
 * @method DOMElement area(string[] $attributes)
 * @method DOMElement audio(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement audio(string $selector, string|DOMElement ...$children)
 * @method DOMElement audio(string|DOMElement ...$children)
 * @method DOMElement audio(string[] $attributes)
 * @method DOMElement img(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement img(string $selector, string|DOMElement ...$children)
 * @method DOMElement img(string|DOMElement ...$children)
 * @method DOMElement img(string[] $attributes)
 * @method DOMElement map(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement map(string $selector, string|DOMElement ...$children)
 * @method DOMElement map(string|DOMElement ...$children)
 * @method DOMElement map(string[] $attributes)
 * @method DOMElement track(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement track(string $selector, string|DOMElement ...$children)
 * @method DOMElement track(string|DOMElement ...$children)
 * @method DOMElement track(string[] $attributes)
 * @method DOMElement video(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement video(string $selector, string|DOMElement ...$children)
 * @method DOMElement video(string|DOMElement ...$children)
 * @method DOMElement video(string[] $attributes)
 * @method DOMElement embed(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement embed(string $selector, string|DOMElement ...$children)
 * @method DOMElement embed(string|DOMElement ...$children)
 * @method DOMElement embed(string[] $attributes)
 * @method DOMElement iframe(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement iframe(string $selector, string|DOMElement ...$children)
 * @method DOMElement iframe(string|DOMElement ...$children)
 * @method DOMElement iframe(string[] $attributes)
 * @method DOMElement object(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement object(string $selector, string|DOMElement ...$children)
 * @method DOMElement object(string|DOMElement ...$children)
 * @method DOMElement object(string[] $attributes)
 * @method DOMElement param(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement param(string $selector, string|DOMElement ...$children)
 * @method DOMElement param(string|DOMElement ...$children)
 * @method DOMElement param(string[] $attributes)
 * @method DOMElement picture(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement picture(string $selector, string|DOMElement ...$children)
 * @method DOMElement picture(string|DOMElement ...$children)
 * @method DOMElement picture(string[] $attributes)
 * @method DOMElement source(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement source(string $selector, string|DOMElement ...$children)
 * @method DOMElement source(string|DOMElement ...$children)
 * @method DOMElement source(string[] $attributes)
 * @method DOMElement canvas(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement canvas(string $selector, string|DOMElement ...$children)
 * @method DOMElement canvas(string|DOMElement ...$children)
 * @method DOMElement canvas(string[] $attributes)
 * @method DOMElement noscript(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement noscript(string $selector, string|DOMElement ...$children)
 * @method DOMElement noscript(string|DOMElement ...$children)
 * @method DOMElement noscript(string[] $attributes)
 * @method DOMElement script(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement script(string $selector, string|DOMElement ...$children)
 * @method DOMElement script(string|DOMElement ...$children)
 * @method DOMElement script(string[] $attributes)
 * @method DOMElement del(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement del(string $selector, string|DOMElement ...$children)
 * @method DOMElement del(string|DOMElement ...$children)
 * @method DOMElement del(string[] $attributes)
 * @method DOMElement ins(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement ins(string $selector, string|DOMElement ...$children)
 * @method DOMElement ins(string|DOMElement ...$children)
 * @method DOMElement ins(string[] $attributes)
 * @method DOMElement caption(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement caption(string $selector, string|DOMElement ...$children)
 * @method DOMElement caption(string|DOMElement ...$children)
 * @method DOMElement caption(string[] $attributes)
 * @method DOMElement col(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement col(string $selector, string|DOMElement ...$children)
 * @method DOMElement col(string|DOMElement ...$children)
 * @method DOMElement col(string[] $attributes)
 * @method DOMElement colgroup(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement colgroup(string $selector, string|DOMElement ...$children)
 * @method DOMElement colgroup(string|DOMElement ...$children)
 * @method DOMElement colgroup(string[] $attributes)
 * @method DOMElement table(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement table(string $selector, string|DOMElement ...$children)
 * @method DOMElement table(string|DOMElement ...$children)
 * @method DOMElement table(string[] $attributes)
 * @method DOMElement tbody(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement tbody(string $selector, string|DOMElement ...$children)
 * @method DOMElement tbody(string|DOMElement ...$children)
 * @method DOMElement tbody(string[] $attributes)
 * @method DOMElement td(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement td(string $selector, string|DOMElement ...$children)
 * @method DOMElement td(string|DOMElement ...$children)
 * @method DOMElement td(string[] $attributes)
 * @method DOMElement tfoot(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement tfoot(string $selector, string|DOMElement ...$children)
 * @method DOMElement tfoot(string|DOMElement ...$children)
 * @method DOMElement tfoot(string[] $attributes)
 * @method DOMElement th(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement th(string $selector, string|DOMElement ...$children)
 * @method DOMElement th(string|DOMElement ...$children)
 * @method DOMElement th(string[] $attributes)
 * @method DOMElement thead(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement thead(string $selector, string|DOMElement ...$children)
 * @method DOMElement thead(string|DOMElement ...$children)
 * @method DOMElement thead(string[] $attributes)
 * @method DOMElement tr(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement tr(string $selector, string|DOMElement ...$children)
 * @method DOMElement tr(string|DOMElement ...$children)
 * @method DOMElement tr(string[] $attributes)
 * @method DOMElement button(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement button(string $selector, string|DOMElement ...$children)
 * @method DOMElement button(string|DOMElement ...$children)
 * @method DOMElement button(string[] $attributes)
 * @method DOMElement datalist(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement datalist(string $selector, string|DOMElement ...$children)
 * @method DOMElement datalist(string|DOMElement ...$children)
 * @method DOMElement datalist(string[] $attributes)
 * @method DOMElement fieldset(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement fieldset(string $selector, string|DOMElement ...$children)
 * @method DOMElement fieldset(string|DOMElement ...$children)
 * @method DOMElement fieldset(string[] $attributes)
 * @method DOMElement form(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement form(string $selector, string|DOMElement ...$children)
 * @method DOMElement form(string|DOMElement ...$children)
 * @method DOMElement form(string[] $attributes)
 * @method DOMElement input(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement input(string $selector, string|DOMElement ...$children)
 * @method DOMElement input(string|DOMElement ...$children)
 * @method DOMElement input(string[] $attributes)
 * @method DOMElement label(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement label(string $selector, string|DOMElement ...$children)
 * @method DOMElement label(string|DOMElement ...$children)
 * @method DOMElement label(string[] $attributes)
 * @method DOMElement legend(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement legend(string $selector, string|DOMElement ...$children)
 * @method DOMElement legend(string|DOMElement ...$children)
 * @method DOMElement legend(string[] $attributes)
 * @method DOMElement meter(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement meter(string $selector, string|DOMElement ...$children)
 * @method DOMElement meter(string|DOMElement ...$children)
 * @method DOMElement meter(string[] $attributes)
 * @method DOMElement optgroup(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement optgroup(string $selector, string|DOMElement ...$children)
 * @method DOMElement optgroup(string|DOMElement ...$children)
 * @method DOMElement optgroup(string[] $attributes)
 * @method DOMElement option(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement option(string $selector, string|DOMElement ...$children)
 * @method DOMElement option(string|DOMElement ...$children)
 * @method DOMElement option(string[] $attributes)
 * @method DOMElement output(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement output(string $selector, string|DOMElement ...$children)
 * @method DOMElement output(string|DOMElement ...$children)
 * @method DOMElement output(string[] $attributes)
 * @method DOMElement progress(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement progress(string $selector, string|DOMElement ...$children)
 * @method DOMElement progress(string|DOMElement ...$children)
 * @method DOMElement progress(string[] $attributes)
 * @method DOMElement select(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement select(string $selector, string|DOMElement ...$children)
 * @method DOMElement select(string|DOMElement ...$children)
 * @method DOMElement select(string[] $attributes)
 * @method DOMElement textarea(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement textarea(string $selector, string|DOMElement ...$children)
 * @method DOMElement textarea(string|DOMElement ...$children)
 * @method DOMElement textarea(string[] $attributes)
 * @method DOMElement details(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement details(string $selector, string|DOMElement ...$children)
 * @method DOMElement details(string|DOMElement ...$children)
 * @method DOMElement details(string[] $attributes)
 * @method DOMElement dialog(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement dialog(string $selector, string|DOMElement ...$children)
 * @method DOMElement dialog(string|DOMElement ...$children)
 * @method DOMElement dialog(string[] $attributes)
 * @method DOMElement menu(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement menu(string $selector, string|DOMElement ...$children)
 * @method DOMElement menu(string|DOMElement ...$children)
 * @method DOMElement menu(string[] $attributes)
 * @method DOMElement summary(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement summary(string $selector, string|DOMElement ...$children)
 * @method DOMElement summary(string|DOMElement ...$children)
 * @method DOMElement summary(string[] $attributes)
 * @method DOMElement slot(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement slot(string $selector, string|DOMElement ...$children)
 * @method DOMElement slot(string|DOMElement ...$children)
 * @method DOMElement slot(string[] $attributes)
 * @method DOMElement template(string $selector, string[] $attributes = null, string|DOMElement ...$children)
 * @method DOMElement template(string $selector, string|DOMElement ...$children)
 * @method DOMElement template(string|DOMElement ...$children)
 * @method DOMElement template(string[] $attributes)
 */
class Hyperscript
{
    private DOMDocument $domDocument;

    public function __construct(DOMDocument $domDocument)
    {
        $this->domDocument = $domDocument;
    }

    /**
     * @return DOMDocument
     */
    public function getDomDocument(): DOMDocument
    {
        return $this->domDocument;
    }

    /**
     * valid overloads:
     * @method DOMElement __invoke(string $querySelector, array $attributes = [], string|DOMNode ...$children)
     * @method DOMElement __invoke(string $querySelector, string|DOMNode ...$children)
     *
     * @param string $cssName element name with optional classes or ids, e.g. "div.column#main"
     * @param string[]|string|DOMNode $attributesOrFirstChild
     * @param string|DOMNode  ...$children
     *
     * @return DOMElement
     */
    public function __invoke(string $cssName, $attributesOrFirstChild = [], ...$children): DOMElement
    {
        $matches = Regex::match('/^(?<element>[\w-]+)(?<rest>[.#].*)*/', $cssName);
        if (!$matches->hasMatch()) {
            throw new InvalidElementName("Invalid element '$cssName'");
        }

        $element = $this->domDocument->createElement($matches->group('element'));
        $attributes = self::selectorToAttributes($matches->groupOr('rest', ''));

        if (is_array($attributesOrFirstChild)) {
            foreach ($attributesOrFirstChild as $attrName => $attrValue) {
                $attributes[$attrName][] = $attrValue;
            }
        } else {
            array_unshift($children, $attributesOrFirstChild);
        }

        foreach ($attributes as $attrName => $attrValues) {
            $attrValue = implode(' ', $attrValues);
            $element->setAttribute($attrName, $attrValue);
        }

        foreach ($children as $child) {
            if (is_string($child)) {
                $element->appendChild(new DOMText($child));
            } else if ($child instanceof DOMNode) {
                $element->appendChild($child);
            } else if ($child === null) {
                // ignore, makes it easier to include expressions which may return null
                // so the user does not use empty string which would create a superfluous text node
                continue;
            } else {
                throw new InvalidArgumentException('Child of the wrong type');
            }
        }

        return $element;
    }

    /**
     * @param string|DOMNode  ...$children
     */
    public function fragment(DOMNode ...$children): DOMDocumentFragment
    {
        $fragment = $this->domDocument->createDocumentFragment();
        $fragment->append(...$children);
        return $fragment;
    }

    public function unsafe(string $html): DOMDocumentFragment
    {
        $fragment = $this->domDocument->createDocumentFragment();
        $fragment->appendXML($html);
        return $fragment;
    }

    public function render(DOMNode $element): string
    {
        return $this->domDocument->saveHTML($element);
    }

    /**
     * This allows you to use the HTML tag name as method name
     * e.g. $h->h1('Welcome');
     *
     * @param string $name
     * @param array  $arguments
     */
    public function __call(string $name, array $arguments): DOMElement
    {
        $cssSelector = $name;

        $selectorOrAttributesOrFirstChild = $arguments[0] ?? null;
        $attributesOrFirstChildOrSecondChild = $arguments[1] ?? null;
        $children = array_slice($arguments, 2);

        $args = [];

        if ($selectorOrAttributesOrFirstChild !== null) {
            if (is_string($selectorOrAttributesOrFirstChild)
                && strlen($selectorOrAttributesOrFirstChild) > 1
                && in_array($selectorOrAttributesOrFirstChild[0], ['.', '#'])
            ) {
                $cssSelector .= $selectorOrAttributesOrFirstChild;
            } else {
                $args[] = $selectorOrAttributesOrFirstChild;
            }
        }

        if ($attributesOrFirstChildOrSecondChild !== null) {
            $args[] = $attributesOrFirstChildOrSecondChild;
        }

        return $this->__invoke($cssSelector, ...$args, ...$children);
    }

    /**
     * @param string $cssSelector
     *
     * @return string[][] hashmap of attributes to values
     */
    protected static function selectorToAttributes(string $cssSelector): array
    {
        if ($cssSelector === '') {
            return [];
        }

        // https://stackoverflow.com/a/449000/1899162
        $matches = Regex::matchAll("
            /
                (?<symbol>[.#])
                (?<name>-?[_a-zA-Z]+[_a-zA-Z0-9-]*)
            /x
            ",
            $cssSelector
        );

        $attributes = [];
        foreach ($matches->results() as $matchResult) {
            $symbol = $matchResult->group('symbol');
            $type = self::getType($symbol);

            $attributes[$type][] = $matchResult->group('name');
        }

        return $attributes;
    }

    /**
     * Check if CSS selector is a class or id
     */
    protected static function getType(string $cssSymbol): string
    {
        switch ($cssSymbol) {
            case '.':
                return 'class';
            case '#':
                return 'id';
            default:
                throw new InvalidArgumentException(
                    "Only supported css selectors are classes and ids, got '$cssSymbol'"
                );
        }
    }
}